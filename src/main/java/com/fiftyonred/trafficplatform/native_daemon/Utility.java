package com.fiftyonred.trafficplatform.native_daemon;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utility {

	/**
	 * Generate a timestamp value
	 * @return A timestamp value for saving files
	 */
	public static String getTimeForFile() {
		final Calendar cal = Calendar.getInstance();
		final int minute;

		if (cal.get(Calendar.MINUTE) < 15) {
			minute = 0;
		} else if (cal.get(Calendar.MINUTE) < 30) {
			minute = 15;
		} else if (cal.get(Calendar.MINUTE) < 45) {
			minute = 30;
		} else {
			minute = 45;
		}

		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), minute);

		return new SimpleDateFormat("yyyyMMddHHmm").format(cal.getTime());
	}
}
