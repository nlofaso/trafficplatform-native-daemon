package com.fiftyonred.trafficplatform.native_daemon;

import com.fiftyonred.trafficplatform.native_daemon.logging.EventLogger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@Singleton
public class StartupJob implements Runnable {

	private static final Logger LOGGER = LoggerFactory.getLogger(StartupJob.class);

	private final DaemonScheduler daemonScheduler;

	@Inject
	public StartupJob(DaemonScheduler daemonScheduler) {

		this.daemonScheduler = daemonScheduler;
	}

	@Override
	public void run() {

		LOGGER.debug("Starting DaemonScheduler...");
		daemonScheduler.start();
		LOGGER.debug("DaemonScheduler started.");

	}
}