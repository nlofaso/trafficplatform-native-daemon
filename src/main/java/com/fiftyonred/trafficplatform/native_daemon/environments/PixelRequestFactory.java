package com.fiftyonred.trafficplatform.native_daemon.environments;

import com.fiftyonred.trafficplatform.native_daemon.common.db.GeoRepository;
import com.fiftyonred.trafficplatform.native_daemon.common.models.CachedUserAgentStringParser;
import com.fiftyonred.trafficplatform.native_daemon.common.models.CachedWurflDeviceParser;
import com.fiftyonred.trafficplatform.native_daemon.common.models.GeoEntry;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.servlet.http.HttpServletRequest;

@Singleton
public class PixelRequestFactory {

	private final CachedUserAgentStringParser userAgentParser;
	private final CachedWurflDeviceParser cachedWurflDeviceParser;
	private final GeoRepository geoRepository;

	@Inject
	public PixelRequestFactory(CachedUserAgentStringParser userAgentParser,
							   CachedWurflDeviceParser cachedWurflDeviceParser,
							   GeoRepository geoRepository) {
		this.userAgentParser = userAgentParser;
		this.cachedWurflDeviceParser = cachedWurflDeviceParser;
		this.geoRepository = geoRepository;
	}

	public PixelRequest createPixelRequest(HttpServletRequest request) {
		String ip = request.getParameter("ip");

		UserAgent userAgent = new UserAgent(userAgentParser, cachedWurflDeviceParser, getUserAgentString(request));
		GeoEntry geoEntry = geoRepository.getGeoEntry(ip);
		return new PixelRequest(userAgent, geoEntry);
	}

	public static String getUserAgentString(final HttpServletRequest request) {
		final String ua = request.getHeader("User-Agent");
		return ua == null ? "" : ua;
	}
}
