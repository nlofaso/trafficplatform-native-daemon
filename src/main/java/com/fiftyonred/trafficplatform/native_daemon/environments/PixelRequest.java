package com.fiftyonred.trafficplatform.native_daemon.environments;

import com.fiftyonred.trafficplatform.native_daemon.common.models.GeoEntry;

public class PixelRequest {

	private final UserAgent userAgent;


	private final GeoEntry geoEntry;

	public PixelRequest(UserAgent userAgent, GeoEntry geoEntry) {
		this.userAgent = userAgent;
		this.geoEntry = geoEntry;
	}

	public UserAgent getUserAgent() {
		return userAgent;
	}

	public GeoEntry getGeoEntry() {
		return geoEntry;
	}

}
