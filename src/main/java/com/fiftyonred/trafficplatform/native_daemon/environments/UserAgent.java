package com.fiftyonred.trafficplatform.native_daemon.environments;

import com.fiftyonred.trafficplatform.native_daemon.common.models.CachedUserAgentStringParser;
import com.fiftyonred.trafficplatform.native_daemon.common.models.CachedWurflDeviceParser;
import com.google.common.base.Optional;
import com.scientiamobile.wurfl.core.Device;
import net.sf.uadetector.ReadableUserAgent;

public class UserAgent {
	private final String userAgentStr;
	private final CachedUserAgentStringParser userAgentParser;
	private final ReadableUserAgent readableUserAgent;
	private String os;
	private String rawOs;
	private String browser;
	private String browserFamily;
	private String deviceType;
	private String deviceName;
	private String rawDeviceType;
	private boolean isMobile;

	public UserAgent(CachedUserAgentStringParser userAgentParser, CachedWurflDeviceParser cachedWurflDeviceParser, String userAgentStr) {
		this.userAgentStr = userAgentStr;
		this.userAgentParser = userAgentParser;
		this.readableUserAgent = userAgentParser.parse(userAgentStr);
		Optional<Device> device = cachedWurflDeviceParser.getDevice(userAgentStr);
		this.deviceName = device.isPresent() ? device.get().getCapability("device_name").toLowerCase() : "";
	}

	public String getDeviceName() {
		return deviceName;
	}


}
