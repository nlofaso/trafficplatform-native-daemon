package com.fiftyonred.trafficplatform.native_daemon;


import com.fiftyonred.trafficplatform.native_daemon.logging.EventLogger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Class which starts a thread which reloads manifest at regular intervals NOT
 * thread-safe
 */
@Singleton
class DaemonScheduler {
	private static final Logger LOGGER = LoggerFactory.getLogger(DaemonScheduler.class);
	private static final int NUM_THREADS = 2;

	private ScheduledFuture<?> loggingEvent = null;
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(NUM_THREADS);


	private final List<Runnable> loggingRunnables;

	@Inject
	public DaemonScheduler(@Named("logging.runnables") List<Runnable> loggingRunnables) {

		this.loggingRunnables = loggingRunnables;

	}

	/**
	 * Stop the scheduled events
	 */
	public void stop() {

	}

	/**
	 * Rebuild the trees at a regular intervals
	 */
	public void start() {

		Runnable loggingRunnable = () -> {
			for (Runnable r : loggingRunnables) {
				try {
					r.run();
				} catch (Throwable e) {
					LOGGER.error("Failed to write to action logs.", e);
				}
			}
		};

		loggingEvent = scheduler.scheduleWithFixedDelay(loggingRunnable, 3, 1, TimeUnit.SECONDS);

	}
}
