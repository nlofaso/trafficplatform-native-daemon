package com.fiftyonred.trafficplatform.native_daemon.common.db;

import com.codahale.metrics.annotation.Timed;
import com.fiftyonred.trafficplatform.native_daemon.common.models.GeoEntry;
import com.google.common.net.InetAddresses;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.maxmind.db.CHMCache;
import com.maxmind.db.Reader;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

@Singleton
public class GeoRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(GeoRepository.class);
    private final DatabaseReader reader;

    @Inject
    public GeoRepository(@Named("geo.geoip2.city_path") String databasePath,
						 @Named("geo.cache_size") int cacheSize) throws IOException {
        this.reader = new DatabaseReader.Builder(new File(databasePath))
                .fileMode(Reader.FileMode.MEMORY)
                .withCache(new CHMCache(cacheSize))
                .build();
    }

    @Timed
    public GeoEntry getGeoEntry(String ipAddress) {
        if (ipAddress == null) {
            return GeoEntry.getEmptyGeoEntry();
        }

        try {
            InetAddress inetAddr = InetAddresses.forString(ipAddress);
            CityResponse response = reader.city(inetAddr);
            if (response != null && response.getCountry() != null && response.getCountry().getIsoCode() != null) {
                return GeoEntry.getOrCreate(StringUtils.stripAccents(response.getCountry().getIsoCode()), StringUtils.stripAccents(response.getMostSpecificSubdivision().getName()),
                        StringUtils.stripAccents(response.getCity().getName()));
            }
        } catch (GeoIp2Exception | IOException e) {
            LOGGER.debug("Unable to get geo for IP: " + ipAddress);
        }

        return GeoEntry.getEmptyGeoEntry();
    }
}
