package com.fiftyonred.trafficplatform.native_daemon.common.models;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;

@Singleton
public class CachedUserAgentStringParser implements UserAgentStringParser {

	private final UserAgentStringParser parser;
	private final Cache<String, ReadableUserAgent> cache;

	@Inject
	private CachedUserAgentStringParser() {
		parser = UADetectorServiceFactory.getResourceModuleParser();
		cache = CacheBuilder.newBuilder()
				.maximumSize(200000)
				.build();
	}

	@Override
	public String getDataVersion() {
		return parser.getDataVersion();
	}

	@Override
	public ReadableUserAgent parse(String userAgent) {
		if (userAgent == null) {
			return null;
		}

		ReadableUserAgent result = cache.getIfPresent(userAgent);
		if (result == null) {
			result = parser.parse(userAgent);
			cache.put(userAgent, result);
		}
		return result;
	}

	@Override
	public void shutdown() {
		parser.shutdown();
	}
}
