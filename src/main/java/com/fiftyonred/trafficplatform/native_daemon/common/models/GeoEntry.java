package com.fiftyonred.trafficplatform.native_daemon.common.models;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public final class GeoEntry {
	private final String country;
	@Nullable private final String region;
	@Nullable private final String city;
	private final GeoTier tier;

	private final static Map<String, GeoEntry> GEO_ENTRY_CACHE = new ConcurrentHashMap<>();
	
	public GeoEntry(final String country, @Nullable final String region, @Nullable final String city) {
		Preconditions.checkNotNull(country, "Country was null");
		this.country = country;
		this.region = region;
		this.city = city;
		this.tier = GeoTier.getTier(country);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (!(o instanceof GeoEntry)) return false;

		final GeoEntry geoEntry = (GeoEntry) o;

		if (city != null ? !city.equals(geoEntry.city) : geoEntry.city != null) return false;
		if (country != null ? !country.equals(geoEntry.country) : geoEntry.country != null) return false;
		return !(region != null ? !region.equals(geoEntry.region) : geoEntry.region != null);
	}

	@Override
	public int hashCode() {
		int result = country != null ? country.hashCode() : 0;
		result = 31 * result + (region != null ? region.hashCode() : 0);
		result = 31 * result + (city != null ? city.hashCode() : 0);
		return result;
	}

	public boolean contains(@Nullable final String country, @Nullable final String region, @Nullable final String city) {
		if (this.country != null && !this.country.equals(country)) {
			return false;
		}
		if (this.region != null && !this.region.equals(region)) {
			return false;
		}
		if (this.city != null && !this.city.equals(city)) {
			return false;
		}

		return true;
	}

	public String getCountry() {
		return country;
	}

	@Nullable
	public String getRegion() {
		return region;
	}

	@Nullable
	public String getCity() {
		return city;
	}

	public GeoTier getTier() {
		return tier;
	}

	public Optional<GeoEntry> getParentGeo() {
		if (!Strings.isNullOrEmpty(city)) {
			return Optional.of(GeoEntry.getOrCreate(country, region, null));
		}

		if (!Strings.isNullOrEmpty(region)) {
			return Optional.of(GeoEntry.getOrCreate(country, null, null));
		}

		return Optional.empty();
	}

	public static GeoEntry getOrCreate(final String country, @Nullable final String region, @Nullable final String city) {
		Preconditions.checkNotNull(country, "country was null");
		String key = String.format("%s||%s||%s", Strings.nullToEmpty(country), Strings.nullToEmpty(region), Strings.nullToEmpty(city));
		return GEO_ENTRY_CACHE.computeIfAbsent(key, s -> new GeoEntry(country, region, city));
	}

	public static GeoEntry getEmptyGeoEntry() {
		return new GeoEntry("", "", "");
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("country", country)
				.add("region", region)
				.add("city", city)
				.toString();
	}
}
