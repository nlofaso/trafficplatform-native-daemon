package com.fiftyonred.trafficplatform.native_daemon.common.models;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

public enum GeoTier {
	TIER_1, TIER_2, TIER_3;

	private static final String US_COUNTRY_CODE = "US";
	private static final Set<String> TIER_2_COUNTRIES = ImmutableSet.of("AU", "CA", "FR", "IT", "GB");

	public static GeoTier getTier(GeoEntry geoEntry) {
		return getTier(geoEntry.getCountry());
	}

	public static GeoTier getTier(String country) {
		Preconditions.checkNotNull(country, "Country cannot be null");
		if (US_COUNTRY_CODE.equals(country)) {
			return TIER_1;
		}

		if (TIER_2_COUNTRIES.contains(country)) {
			return TIER_2;
		}

		return TIER_3;
	}
}
