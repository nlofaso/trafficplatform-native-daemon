package com.fiftyonred.trafficplatform.native_daemon.common.models;

import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.scientiamobile.wurfl.core.Device;
import com.scientiamobile.wurfl.core.EngineTarget;
import com.scientiamobile.wurfl.core.GeneralWURFLEngine;

@Singleton
public class CachedWurflDeviceParser {
	private final GeneralWURFLEngine wurflParser;
	private final Cache<String, Device> cache;

	@Inject
	private CachedWurflDeviceParser(@Named("wurfl.db_location") final String wurflDbLocation) {
		wurflParser = new GeneralWURFLEngine(wurflDbLocation);
		wurflParser.setEngineTarget(EngineTarget.performance);
		cache = CacheBuilder.newBuilder()
				.maximumSize(20000)
				.build();
	}

	public Optional<Device> getDevice(String userAgent) {
		if (userAgent == null) {
			return null;
		}

		Optional<Device> result = Optional.fromNullable(cache.getIfPresent(userAgent));
		if (!result.isPresent()) {
			result = Optional.of(wurflParser.getDeviceForRequest(userAgent));
			cache.put(userAgent, result.get());
		}
		return result;
	}


}
