package com.fiftyonred.trafficplatform.native_daemon.jetty;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.codahale.metrics.jetty9.InstrumentedHandler;
import com.codahale.metrics.servlet.InstrumentedFilter;
import com.codahale.metrics.servlets.HealthCheckServlet;
import com.codahale.metrics.servlets.MetricsServlet;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.servlet.GuiceFilter;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

public class JettyModule extends AbstractModule {

	@Override
	protected void configure() {
	}

	@Provides
	@Singleton
	Handler provideHandler(MetricRegistry metricRegistry,
	                       HealthCheckRegistry healthCheckRegistry) {
		ServletContextHandler servletContextHandler = new ServletContextHandler();
		servletContextHandler.setAttribute(MetricsServlet.METRICS_REGISTRY, metricRegistry);
		servletContextHandler.setAttribute(InstrumentedFilter.REGISTRY_ATTRIBUTE, metricRegistry);
		servletContextHandler.setAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY, healthCheckRegistry);
		servletContextHandler.addFilter(GuiceFilter.class, "/*", null);
		servletContextHandler.addServlet(DefaultServlet.class, "/");

		InstrumentedHandler instrumentedHandler = new InstrumentedHandler(metricRegistry, "jetty_handler");
		instrumentedHandler.setHandler(servletContextHandler);
		return instrumentedHandler;
	}

	@Provides
    @Singleton
    Server provideServer(@Named("jetty.server.port") Integer port,
						 @Named("jetty.server.max_threads") Integer maxThreads,
						 @Named("jetty.server.min_threads") Integer minThreads,
						 @Named("jetty.server.idle_timeout") Integer idleTimeout,
                         @Named("jetty.server.stop_timeout") Integer stopTimeout,
                         @Named("jetty.server.request_header_size") Integer requestHeaderSize,
                         Handler handler) {

		QueuedThreadPool threadPool = new QueuedThreadPool(maxThreads, minThreads, idleTimeout);
		threadPool.setName("jetty_server_pool");
        Server server = new Server(threadPool);
        server.setStopAtShutdown(true);
        server.setStopTimeout(stopTimeout);

        HttpConfiguration httpConfiguration = new HttpConfiguration();
        httpConfiguration.setSendServerVersion(false);
        httpConfiguration.setSendDateHeader(false);
        httpConfiguration.setRequestHeaderSize(requestHeaderSize);

        ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(httpConfiguration));
        connector.setPort(port);
        connector.setIdleTimeout(idleTimeout);
        server.setConnectors(new ServerConnector[]{connector});
        server.setHandler(handler);

        return server;
    }

}
