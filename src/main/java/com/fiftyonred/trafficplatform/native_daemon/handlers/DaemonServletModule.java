package com.fiftyonred.trafficplatform.native_daemon.handlers;

import com.codahale.metrics.servlet.InstrumentedFilter;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Scopes;
import com.google.inject.servlet.ServletModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class DaemonServletModule extends ServletModule {
	final static Logger LOGGER = LoggerFactory.getLogger(DaemonServletModule.class);

	@Override
	public void configureServlets() {
		LOGGER.info("configuring pixel servlet");
		bind(PixelDataHandler.class).in(Scopes.SINGLETON);
		instrumentAndServe("/pixel").with(PixelDataHandler.class);
	}

	private ServletKeyBindingBuilder instrumentAndServe(String...routes) {
		for(String route: routes) {
			filter(route).through(new InstrumentedFilter(), ImmutableMap.of("name-prefix", route.split("/")[1]));
		}

		// Method definition looks for serve(String url, String... other urls)
		return routes.length > 1 ? serve(routes[0], Arrays.copyOfRange(routes, 1, routes.length)) : serve(routes[0]);
	}
}