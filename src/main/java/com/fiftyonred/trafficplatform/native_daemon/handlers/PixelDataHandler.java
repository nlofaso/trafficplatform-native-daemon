package com.fiftyonred.trafficplatform.native_daemon.handlers;

import com.fiftyonred.trafficplatform.native_daemon.environments.PixelRequest;
import com.fiftyonred.trafficplatform.native_daemon.environments.PixelRequestFactory;
import com.fiftyonred.trafficplatform.native_daemon.logging.EventLogger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Singleton
public class PixelDataHandler extends HttpServlet {
	final static Logger LOGGER = LoggerFactory.getLogger(PixelDataHandler.class);

	private final PixelRequestFactory pixelRequestFactory;
	private final EventLogger eventLogger;

	@Inject
	public PixelDataHandler(PixelRequestFactory pixelRequestFactory, EventLogger eventLogger) {
		this.pixelRequestFactory = pixelRequestFactory;
		this.eventLogger = eventLogger;
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("Pixel Service Reached!");
		PixelRequest pixelRequest = pixelRequestFactory.createPixelRequest(request);

		//save to log
		eventLogger.logClickPixel(pixelRequest);

		//reroute to final destination
		response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().write("<html><body>Hello World</body></html>");
	}
}
