package com.fiftyonred.trafficplatform.native_daemon;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.List;

@Singleton
public class ShutdownJob extends Thread {

	private static final Logger LOGGER = LoggerFactory.getLogger(StartupJob.class);

	private final DaemonScheduler daemonScheduler;
	private final List<Runnable> loggingRunnables;


	@Inject
	public ShutdownJob(DaemonScheduler daemonScheduler,
					   @Named("logging.runnables")  List<Runnable> loggingRunnables) {
		this.daemonScheduler = daemonScheduler;
		this.loggingRunnables = loggingRunnables;

	}

	@Override
	public void run() {
		// Close background threads, database pools, etc here

		LOGGER.debug("Stopping DaemonScheduler...");
		daemonScheduler.stop();
		LOGGER.debug("DaemonScheduler stopped.");

		// Do one last write of impressions
		for (Runnable r : loggingRunnables) {
			r.run();
		}
	}
}
