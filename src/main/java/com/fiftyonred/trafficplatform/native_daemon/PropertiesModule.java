package com.fiftyonred.trafficplatform.native_daemon;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

import javax.annotation.Nullable;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesModule extends AbstractModule {
	private Properties properties;

	public PropertiesModule(String path, @Nullable String defaultsResourceName) {
		this.properties = loadProperties(path, defaultsResourceName);
	}

	@Override
	protected void configure() {
		Names.bindProperties(binder(), this.properties);
	}

	public Properties loadProperties(String path, @Nullable String defaultsResourceName) {
		Properties newProperties = new Properties();
		if(defaultsResourceName != null) {
			try(InputStream defaultInputStream = getClass().getResourceAsStream(defaultsResourceName);) {
				newProperties.load(defaultInputStream);
			}
			catch (IOException ie) {
				throw new RuntimeException(ie);
			}
		}

		try(InputStream is = new FileInputStream(path)) {
			newProperties.load(is);
		} catch (IOException ie) {
			throw new RuntimeException(ie);
		}

		return newProperties;
	}
}