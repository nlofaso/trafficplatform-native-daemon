package com.fiftyonred.trafficplatform.native_daemon;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import org.eclipse.jetty.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  Main class that starts embedded jetty instance
 */
@Singleton
public class DaemonServer {
	final static Logger LOGGER = LoggerFactory.getLogger(DaemonServer.class);

	private final Server jettyServer;
	private final StartupJob startupJob;
	private final ShutdownJob shutdownJob;

	@Inject
	public DaemonServer(Server jettyServer,
						StartupJob startupJob,
						ShutdownJob shutdownJob) {
		this.jettyServer = jettyServer;
		this.startupJob = startupJob;
		this.shutdownJob = shutdownJob;
	}

	public void init() throws Exception {
		LOGGER.info("Initializing Daemon...");

		Runtime.getRuntime().addShutdownHook(shutdownJob);
		startupJob.run();

		LOGGER.info("Daemon initialized.");
	}

	public void run() throws Exception {
		try {
			init();
			jettyServer.start();
			jettyServer.join();
		} catch (Exception e) {
			LOGGER.error("Exception starting server.", e);
			throw e;
		}
	}

	public static void main(String[] args) throws Exception {
		// Initialize via Guice
		if (args.length < 1) {
			LOGGER.error("Error: you must provide a configuration file");
			System.exit(1);
		}

		// HACK - This should be somewhere else
		java.security.Security.setProperty("networkaddress.cache.ttl" , "30");

		Injector propsInjector = Guice.createInjector(new PropertiesModule(args[0], "/native-daemon-defaults.properties"));
		Injector mainInjector = propsInjector.createChildInjector(new DaemonModule());
		DaemonServer daemon = mainInjector.getInstance(DaemonServer.class);
		daemon.run();
	}
}
