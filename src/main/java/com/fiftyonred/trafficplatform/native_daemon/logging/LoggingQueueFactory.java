package com.fiftyonred.trafficplatform.native_daemon.logging;

import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import java.util.concurrent.LinkedBlockingQueue;

@Singleton
public class LoggingQueueFactory {
	private static final int QUEUE_SIZE = 500000;

	// Queues
	private final LinkedBlockingQueue<JsonObject> impressionPixelLoggingQueue;

	private final LinkedBlockingQueue<JsonObject> clickPixelLoggingQueue;


	// Queue processors
	private final LoggingRunnable<JsonObject> impressionPixelLoggingRunnable;
	private final LoggingRunnable<JsonObject> clickPixelLoggingRunnable;

	@Inject
	public LoggingQueueFactory(@Named("action_log_path") String actionLogPath) {
		impressionPixelLoggingQueue = new LinkedBlockingQueue<>(QUEUE_SIZE);
		clickPixelLoggingQueue = new LinkedBlockingQueue<>(QUEUE_SIZE);

		impressionPixelLoggingRunnable = new LoggingRunnable<>(
				impressionPixelLoggingQueue, actionLogPath, "impression_pixel_requests");

		clickPixelLoggingRunnable = new LoggingRunnable<>(
				clickPixelLoggingQueue, actionLogPath, "click_pixel_requests");

	}

	public LinkedBlockingQueue<JsonObject> getImpressionPixelLoggingQueue() {
		return impressionPixelLoggingQueue;
	}

	public LinkedBlockingQueue<JsonObject> getClickPixelLoggingQueue() { return clickPixelLoggingQueue; }

	public LoggingRunnable<JsonObject> getImpressionPixelLoggingQueueRunnable() { return impressionPixelLoggingRunnable; }

	public LoggingRunnable<JsonObject> getClickPixelLoggingQueueRunnable() { return clickPixelLoggingRunnable; }
}
