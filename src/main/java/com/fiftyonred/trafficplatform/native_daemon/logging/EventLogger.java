package com.fiftyonred.trafficplatform.native_daemon.logging;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.fiftyonred.trafficplatform.native_daemon.environments.PixelRequest;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.slf4j.LoggerFactory;
import java.util.concurrent.LinkedBlockingQueue;

@Singleton
public class EventLogger {
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(EventLogger.class);

	private final LinkedBlockingQueue<JsonObject> impressionPixelLoggingQueue;
	private final LinkedBlockingQueue<JsonObject> clickPixelLoggingQueue;

	@Inject
	public EventLogger(@Named("logging.impression_pixel_logging_queue") LinkedBlockingQueue<JsonObject> impressionPixelLoggingQueue,
					   @Named("logging.click_pixel_logging_queue") LinkedBlockingQueue<JsonObject> clickPixelLoggingQueue
					   ) {
		this.impressionPixelLoggingQueue = impressionPixelLoggingQueue;
		this.clickPixelLoggingQueue = clickPixelLoggingQueue;
	}



	@Timed @ExceptionMetered
	public void logClickPixel(PixelRequest pixelRequest) {
		LOGGER.info("UserAgent: " + pixelRequest.getUserAgent().getDeviceName());
		JsonObject json = new JsonObject();
		json.addProperty("testing", "this is a test");
		json.addProperty("ip country", pixelRequest.getGeoEntry().getCountry());
		json.addProperty("ip region", pixelRequest.getGeoEntry().getRegion());
		json.addProperty("ip city", pixelRequest.getGeoEntry().getCity());
		json.addProperty("device", pixelRequest.getUserAgent().getDeviceName());
		if (!clickPixelLoggingQueue.offer(json)) {
			LOGGER.info("Could not enqueue click pixel result. Queue is full.");
		}
	}


}
