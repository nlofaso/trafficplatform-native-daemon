package com.fiftyonred.trafficplatform.native_daemon.logging;

import com.fiftyonred.trafficplatform.native_daemon.Utility;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class LoggingRunnable<T> implements Runnable {

	private static final Gson gson = new Gson();
	private static final Logger logger = Logger.getLogger(LoggingRunnable.class.getName());
	private static final int BUFFER_SIZE = 1024 * 500;

	private final BlockingQueue<T> queue;
	private final String logLocation;
	private final String logPrefix;
	private final AtomicInteger lastDrainSize = new AtomicInteger(0);

	public LoggingRunnable(final BlockingQueue<T> queue, final String logLocation, final String logPrefix) {
		this.queue = queue;
		this.logLocation = logLocation;
		this.logPrefix = logPrefix;
	}

	@Override
	public void run() {
		synchronized (this) {
			final List<T> messages = new ArrayList<>(queue.size());
			queue.drainTo(messages);

			lastDrainSize.set(messages.size());

			if (messages.isEmpty()) {
				return;
			}

			boolean isString = messages.get(0) instanceof String;

			final String fileName = logLocation + logPrefix + '.' + Utility.getTimeForFile() + ".log";
			try (
					FileOutputStream fileOutputStream = new FileOutputStream(fileName, true);
					OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF-8");
					Writer writer = new BufferedWriter(outputStreamWriter, BUFFER_SIZE)
			) {
				for (final T message : messages) {
					if (isString) {
						writer.write(((String) message) + '\n');
					} else {
						writer.write(gson.toJson(message) + '\n');
					}
				}
			} catch (final IOException e) {
				logger.error("Error writing messages to " + fileName, e);
			}
		}

	}
	
	public int getLastEntryCount() {
		return lastDrainSize.get();
	}
}
