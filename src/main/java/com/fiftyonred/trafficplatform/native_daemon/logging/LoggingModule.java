package com.fiftyonred.trafficplatform.native_daemon.logging;

import com.google.gson.JsonObject;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class LoggingModule extends AbstractModule {

	@Override
	protected void configure() {
	}

	@Provides @Singleton @Named("logging.impression_pixel_logging_queue")
	LinkedBlockingQueue<JsonObject> provideImpressionPixelLoggingQueue(LoggingQueueFactory loggingQueueFactory) {
		return loggingQueueFactory.getImpressionPixelLoggingQueue();
	}

	@Provides @Singleton @Named("logging.click_pixel_logging_queue")
	LinkedBlockingQueue<JsonObject> provideClickPixelLoggingQueue(LoggingQueueFactory loggingQueueFactory) {
		return loggingQueueFactory.getClickPixelLoggingQueue();
	}


	@Provides @Singleton @Named("logging.runnables")
	List<Runnable> provideLoggingRunnables(LoggingQueueFactory loggingQueueFactory) {
		return Arrays.asList(
				loggingQueueFactory.getImpressionPixelLoggingQueueRunnable(),
				loggingQueueFactory.getClickPixelLoggingQueueRunnable()
		);
	}
}
