package com.fiftyonred.trafficplatform.native_daemon;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.fiftyonred.trafficplatform.native_daemon.handlers.DaemonServletModule;
import com.fiftyonred.trafficplatform.native_daemon.jetty.JettyModule;
import com.fiftyonred.trafficplatform.native_daemon.logging.LoggingModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;


public class DaemonModule extends AbstractModule {
	@Override
	protected void configure() {
		install(new JettyModule());
		install(new DaemonServletModule());
		install(new LoggingModule());
	}

	@Provides @Singleton
	HealthCheckRegistry provideHealthCheckRegistry() {
		return new HealthCheckRegistry();
	}

	@Provides @Singleton
	Gson provideGsonInstance() {
		return new GsonBuilder()
				.create();
	}

}
